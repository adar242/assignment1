#include "../include/Dish.h"
#include <vector>
#include <string>

// constructor
Dish::Dish(int d_id, std::string d_name, int d_price, DishType d_type) :
    id(d_id),name(d_name),price(d_price),type(d_type){}

//copy contructor
Dish::Dish(const Dish& aDish): 
id(aDish.getId()),name(aDish.getName()),price(aDish.getPrice()),type(aDish.getType())
{}


int Dish::getId() const{
    return id;
}

std::string Dish::getName() const{
    return name;
}

int Dish::getPrice() const{
    return price;
}


DishType Dish::getType() const{
    return type;
}










    




