#ifndef TABLE_H_
#define TABLE_H_

#include <vector>
#include "Customer.h"
#include "Dish.h"

typedef std::pair<int, Dish> OrderPair;

class Table{
public:
    Table(int t_capacity);
    ~Table();
    Table(Table&& other);
    Table(const Table& other);
    Table& operator= (const Table& other);
    Table& operator= (Table&& other);
    int getCapacity() const;
    void addCustomer(Customer* customer);
    void removeCustomer(int id);
    Customer* getCustomer(int id);
    std::vector<Customer*>& getCustomers();
    std::vector<OrderPair>& getOrders();
    void order(const std::vector<Dish> &menu);
    void openTable();
    void closeTable();
    int getBill();
    bool isOpen();
    void addOrders(std::vector <std::pair<int,Dish>> orders); //add the given orders to orderList
    std::vector <std::pair<int,Dish>> removeOrders (int id); //removes and returns the orders on the given id
private:
    int capacity;
    bool open;
    int bill;
    std::vector<Customer*> customersList;
    std::vector<OrderPair> orderList; //A list of pairs for each order in a table - (customer_id, Dish)
    int dishNum; //counts the amount of dishes ordered in this table
};


#endif
