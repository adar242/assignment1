#include "../include/Restaurant.h"
#include "../include/Dish.h"
#include "../include/Table.h"
#include "../include/Action.h"
#include "../include/Customer.h"

#include <vector>
#include <string>
#include<istream>
#include <iterator>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iostream>

using namespace std;


//copy constructor for creating new tables and new actionslog
Restaurant::Restaurant(const Restaurant& other): open(other.isOpen()), userInput(other.userInput),tables(), menu(other.menu),actionsLog(), idCounter(other.idCounter){
    //creating new tables and pointers for them based on other' tables list
    for (int i = 0; i < other.getNumOfTables(); ++i)
    {
        Table* p_table = new Table(*other.getTables().at(i));
        tables.push_back(p_table);
    }

    //creating new base actions and pointers for them based on other' actionslog list
    for (unsigned int i = 0; i < other.getActionsLog().size(); ++i)
    {
        BaseAction* p_action = other.getActionsLog().at(i) -> clone(); //using BaseAction pure-virtual method clone() to get the pointer of a new specific acion
        this -> actionsLog.push_back(p_action);

    }
}

// Move Constructor
Restaurant::Restaurant(Restaurant&& other) 
    : open(other.isOpen()),userInput(other.userInput), tables(other.tables), menu(other.menu),
    actionsLog(other.actionsLog), idCounter(other.idCounter)
{
    //cleaning table pointers
    for (int i = 0; i < other.getNumOfTables(); ++i){
        other.tables.at(i) = nullptr;
    }

    //cleaning actions pointers
    for (unsigned int i = 0; i < other.getActionsLog().size(); ++i){
        other.actionsLog.at(i) = nullptr;
    }
    
}

//constructor that receives the config file path as argument, and init the restaurant
Restaurant::Restaurant(const std::string &configFilePath):open(false),userInput(),tables(),menu(),actionsLog(), idCounter(0) {
    ifstream myfile (configFilePath);
    string line;
    cout << "constructed a restaurant" <<endl;
    cout << configFilePath << endl;
    

    if (myfile.is_open()){
        cout << "ifstream is open" << endl;
        int id(0); //id for the dishes
        int counter(0); //counter to validate getting the current setting for each field
        while ( getline (myfile,line)  )
        {
            if (line[0] == '#'){ //this line doesnt include config setting
                continue;
            }

            if (counter == 0){ //this line include number of tables in restaurant
                //int numOfTables(stoi(line,nullptr));
                counter++;
                continue;
            }

            if (counter == 1){ //this line include number of seats for each table
                std::stringstream streamer(line);
                string segment;
                vector<int> seglist;
                while (std::getline(streamer,segment,',')){
                    seglist.push_back(stoi(segment));
                }
                
                for (unsigned int i = 0; i < seglist.size(); ++i){
                    Table* p_table = new Table(seglist.at(i));
                    tables.push_back(p_table);
                }
                counter++;
                continue;
            }

            if (counter == 2){ //this line include a dish in menu
                //dividing the line to vector seglist such that seglist={"foodname","food code","price"}
                std::stringstream streamer(line);
                string segment;
                vector<string> seglist;
                while (std::getline(streamer,segment,',')){
                    seglist.push_back(segment);
                }

                int price = stoi(seglist[2]);
                DishType type = convert(seglist[1]);
                menu.push_back(Dish(id,seglist[0],price,type));
                id++;
            }

        }
        myfile.close();
    }

    else cout << "unable to open file";
}

//destructor to clear actionsLog and tables.
Restaurant::~Restaurant(){
    //clearing actionsLog vector
    for (unsigned int i = 0; i < actionsLog.size(); ++i)
    {
        delete actionsLog[i];
        actionsLog[i] = nullptr;
    }

    //clearing tables vector
    for (unsigned int i = 0; i < tables.size(); ++i){
        delete tables[i];
        tables[i] = nullptr;
    }
}

//assignment operator
Restaurant & Restaurant::operator=(const Restaurant &other)
{
    // check for "self assignment" and do nothing in that case
    if (this == &other) {
        return *this;
    }
    this -> open = other.open;
    menu.clear();
    for (unsigned int i = 0; i < other.menu.size(); ++i){
        menu.push_back(Dish(other.menu.at(i)));
    }

    //clearing old tables vector
    for(unsigned int i = 0; i < tables.size(); ++i)
    {
        delete tables.at(i);
        tables.at(i) = nullptr;
    }
    tables.clear();

    //clearing old actionlog vector
    for(unsigned int i = 0; i < actionsLog.size(); ++i)
    {
        delete actionsLog.at(i);
        actionsLog.at(i) = nullptr;
    }
    actionsLog.clear();
    
    //creating new tables and pointers for them based on other' tables list
    for (int i = 0; i < other.getNumOfTables(); ++i)
    {
        Table* p_table = new Table(*other.getTables().at(i));
        tables.push_back(p_table);
        
    }

    //creating new base actions and pointers for them based on other' actionslog list
    for (unsigned int i = 0; i < other.getActionsLog().size(); ++i)
    {
        BaseAction* p_action = other.getActionsLog().at(i) -> clone(); //using BaseAction pure-virtual method clone() to get the pointer of a new specific acion
        this -> actionsLog.push_back(p_action);

    }
    return *this;

}
//move assignment operator
Restaurant & Restaurant::operator=(Restaurant &&other)
{
    // check for "self assignment" and do nothing in that case
    if (this == &other) {
        return *this;
    }

    open = other.open;
    menu.clear();
    for (unsigned int i = 0; i < other.menu.size(); ++i){
        menu.push_back(Dish(other.menu.at(i)));
    }

    //clearing old tables vector and replacing with other
    for(unsigned int i = 0; i < tables.size(); ++i)
    {
        delete tables.at(i);
        tables.at(i) = nullptr;
    }
    tables.clear();
    tables = other.tables;

    //clearing old actionlog vector and replacing with other
    for(unsigned int i = 0; i < actionsLog.size(); ++i)
    {
        delete actionsLog.at(i);
        actionsLog.at(i) = nullptr;
    }
    actionsLog.clear();
    actionsLog = other.actionsLog;

    //setting other's pointers to null
    for (int i = 0; i < other.getNumOfTables(); ++i)
    {
        other.tables.at(i) = nullptr;
    }

    for (unsigned int i = 0; i < other.getActionsLog().size(); ++i)
    {
        other.actionsLog.at(i) = nullptr;

    }

    return *this;
}


//function to convert string to dishtype
DishType Restaurant::convert(string str){
    if (str == "VEG") return VEG;
    if (str == "SPC") return SPC;
    if (str == "BVG") return BVG;
    if (str == "ALC") return ALC;
    return VEG;
}


void Restaurant::start(){
    this -> open = true;
    cout << "Restaurant is now open!" << endl;
    cout << "please enter an action: " << endl;
    string action;
    while (true){
        getline(cin,action);
        userInput = action;
        if (action == "closeall"){
            CloseAll* closeallAction = new CloseAll();
            actionsLog.push_back(closeallAction);
            closeallAction -> act(*this);
            open = false;
            cout << "Restaurant is now closed, bye!" << endl;
            return;
        }
        takeAction(action);
        cout << "please enter another action: " << endl;
    }
}

string Restaurant::get_userInput() const{
    return this -> userInput;
}

bool Restaurant::isOpen() const{
    return this->open;
}


int Restaurant::getNumOfTables() const{
    return tables.size();
}

const vector<Table*>& Restaurant::getTables() const{
    return this -> tables;
}

Table* Restaurant::getTable(int ind){
    Table* ret = tables.at(ind);
    return ret;
}


const std::vector<BaseAction*>& Restaurant::getActionsLog() const{ // Return a reference to the history of actions
    const std::vector<BaseAction*>& ret = actionsLog;
    return ret;
}

const std::vector<Dish>& Restaurant::getMenu(){
    std::vector<Dish>& ret = menu;
    return ret;
}


void Restaurant::takeAction(string action){
    //first parse the line to decide which command is it
    std::istringstream buf(action);
    istream_iterator<string> beg(buf), end;

    vector<string> inputs(beg,end);  //this vector contains the string split by spaces
    string command = inputs.at(0);
    inputs.erase(inputs.begin());

    //figuring out the command and running it
    if(command == "open"){  //open table command
        startOpenTable(inputs);
        return;
    }
    if(command == "order"){ //order command
        startOrder(inputs);
        return;
    }
    if (command == "move"){
        startMove(inputs);
        return;
    }

    if (command == "close"){
        startClose(inputs);
        return;
    }

    if (command == "menu"){
        startMenu();
        return;
    }

    if (command == "status"){
        startStatus(inputs);
        return;
    }

    if (command == "log"){
        startLog();
        return;
    }

    if(command == "backup"){
        startBackup();
        return;
    }

    if (command == "restore"){
        startRestore();
        return;
    }




}
//create Action object of openTable using list of inputs from command. use it's act() nethod and add it to action log
void Restaurant::startOpenTable(vector<string> inputs){ 
    int tableId = stoi(inputs.at(0)); //getting table id from first slot
    inputs.erase(inputs.begin()); //and removing the id from the inputs vector

    vector<Customer *> customers;
    for (unsigned int i = 0; i < inputs.size(); ++i){  //each cell contains a string formatted such: "name,type"
        stringstream ss(inputs.at(i));
        vector<string> nameAndType;  //the vector to split the string into

        while( ss.good() ){  //nameAndType[0] = the customer name,  nameAndType[1] = the Customer type
            string substr;
            getline( ss, substr, ',' );
            nameAndType.push_back( substr );
        }

        //creating the customer based on his strategy
        if (nameAndType.at(1) == "veg"){
            customers.push_back(new VegetarianCustomer(nameAndType.at(0),idCounter));
            idCounter++;
            continue;
        }

        if (nameAndType.at(1) == "chp"){
            customers.push_back(new CheapCustomer(nameAndType.at(0),idCounter));
            idCounter++;
            continue;
        }

        if (nameAndType.at(1) == "spc"){
            customers.push_back(new SpicyCustomer(nameAndType.at(0),idCounter));
            idCounter++;
            continue;
        }

        if (nameAndType.at(1) == "alc"){
            customers.push_back(new AlchoholicCustomer(nameAndType.at(0),idCounter));
            idCounter++;
            continue;
        }
    }
    OpenTable* openAction = new OpenTable(tableId,customers);
    actionsLog.push_back(openAction);
    openAction -> act(*this);

}

//create Action object of Order using list of inputs from command. use it's act() nethod and add it to action log
void Restaurant::startOrder(vector<string> inputs){
    Order* orderAction = new Order(stoi(inputs.at(0)));
    actionsLog.push_back(orderAction);
    orderAction -> act(*this);

}

void Restaurant::startMove(vector<string> inputs){ 
    int originId = stoi(inputs.at(0));
    int destId = stoi(inputs.at(1));
    int custId = stoi(inputs.at(2));

    MoveCustomer* moveAction = new MoveCustomer(originId,destId,custId);
    actionsLog.push_back(moveAction);
    moveAction -> act(*this);
}

void Restaurant::startClose(vector<string> inputs){ 
    int tableId = stoi(inputs.at(0));

    Close* closeAction = new Close(tableId);
    actionsLog.push_back(closeAction);
    closeAction -> act(*this);
}

void Restaurant::startMenu(){ 
    PrintMenu* printMenuAction = new PrintMenu();
    actionsLog.push_back(printMenuAction);
    printMenuAction -> act(*this);

}

void Restaurant::startStatus(vector<string> inputs){ 
    int id = stoi(inputs.at(0));

    PrintTableStatus* printTableAction = new PrintTableStatus(id);
    actionsLog.push_back(printTableAction);
    printTableAction -> act(*this);
}

void Restaurant::startLog(){ 
    PrintActionsLog* printLogAction = new PrintActionsLog();
    actionsLog.push_back(printLogAction);
    printLogAction -> act(*this);
}

void Restaurant::startBackup(){ 
    BackupRestaurant* backupAction = new BackupRestaurant();
    actionsLog.push_back(backupAction);
    backupAction -> act(*this);

}

void Restaurant::startRestore(){ 
    RestoreResturant* restoreAction = new RestoreResturant();
    restoreAction -> act(*this);
    actionsLog.push_back(restoreAction);

}
