#ifndef RESTAURANT_H_
#define RESTAURANT_H_

#include <vector>
#include <string>
#include "Dish.h"
#include "Table.h"
#include "Action.h"


class Restaurant{		
public:
	Restaurant();
    ~Restaurant();
    Restaurant(Restaurant&& other);
    Restaurant(const Restaurant& other);
    Restaurant(const std::string &configFilePath);
    Restaurant& operator=(const Restaurant &other);
    Restaurant& operator=( Restaurant &&other);
    void start();
    int getNumOfTables() const;
    Table* getTable(int ind);
	const std::vector<BaseAction*>& getActionsLog() const; // Return a reference to the history of actions
    const std::vector<Dish>& getMenu();
    void takeAction(std::string action);
    void startOpenTable(std::vector<std::string> inputs);  //init an opentable action
    void startOrder(std::vector<std::string> inputs);  //init an Order action
    void startMove(std::vector<std::string> inputs);  //init a Move sutomer action
    void startClose(std::vector<std::string> inputs);  //init a Close action
    void startMenu();  //init a print menu action
    void startStatus(std::vector<std::string> inputs);  //init a print table status action
    void startLog();  //init a print action log action
    void startBackup();  //init a backup action
    void startRestore();  //init a restore action
    std::string get_userInput() const;
    
private:
    DishType convert(std::string str);
    bool isOpen() const;
    bool open;
    std::string userInput;
    std::vector<Table*> tables;
    std::vector<Dish> menu;
    std::vector<BaseAction*> actionsLog;
    const std::vector<Table*>& getTables() const;
    int idCounter; //id for the customers
};

#endif