#ifndef CUSTOMER_H_
#define CUSTOMER_H_

#include <vector>
#include <string>
#include "Dish.h"

class Customer{
public:
    Customer(std::string c_name, int c_id);
    Customer(const Customer& other);
    virtual ~Customer();
    virtual std::vector<int> order(const std::vector<Dish> &menu)=0;
    virtual std::string toString() const = 0;
    std::string getName() const;
    int getId() const;
    int getNumOfOrder() const;
    int getBill() const;
    void addNumOfOrder();
    void addDecision(int id);
    std::vector<int> getDecision() const;
    void addBill(int amount);
    virtual Customer* clone() = 0;
private:
    const std::string name;
    const int id;
    int numOfOrder; //how many times did he ordered
    std::vector<int> decision; //the ids of the dishes the customer whats to do
    int bill; //his personal bill
};


class VegetarianCustomer : public Customer {
public:
	VegetarianCustomer(std::string name, int id);
    ~VegetarianCustomer();
    VegetarianCustomer(const VegetarianCustomer& other);
    std::vector<int> order(const std::vector<Dish> &menu);
    std::string toString() const;
    Customer* clone();
private:
};


class CheapCustomer : public Customer {
public:
	CheapCustomer(std::string name, int id);
    CheapCustomer(const CheapCustomer& other);
    ~CheapCustomer();
    std::vector<int> order(const std::vector<Dish> &menu);
    std::string toString() const;
    Customer* clone();
private:
};


class SpicyCustomer : public Customer {
public:
	SpicyCustomer(std::string name, int id);
    SpicyCustomer(const SpicyCustomer& other);
    ~SpicyCustomer();
    std::vector<int> order(const std::vector<Dish> &menu);
    std::string toString() const;
    Customer* clone();
private:
};


class AlchoholicCustomer : public Customer {
public:
    AlchoholicCustomer(std::string name, int id);
    ~AlchoholicCustomer();
    AlchoholicCustomer(const AlchoholicCustomer& other);
    std::vector<int> order(const std::vector<Dish> &menu);
    std::string toString() const;
    Customer* clone();
private:
    int prev; // the id of previous dish
};


#endif
