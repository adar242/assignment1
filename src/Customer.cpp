#include "../include/Customer.h"
#include "../include/Dish.h"

using std::vector;

//Customer implementation
Customer::Customer(std::string c_name, int c_id): name(c_name), id(c_id), numOfOrder(0), decision(), bill(0) {}

//copy constructor
Customer::Customer(const Customer& other): name(other.name), id(other.id),numOfOrder(other.numOfOrder),
decision(other.decision), bill(other.bill){

}

//destructor
Customer::~Customer() {}

std::string Customer::getName() const {
	return this->name;
}

int Customer::getId() const {
	return this->id;
}

int Customer::getNumOfOrder() const {
	return this->numOfOrder;
}

std::vector<int> Customer::getDecision() const {
	return this->decision;
}

void Customer::addNumOfOrder() {
	this->numOfOrder++;
}

void Customer::addDecision (int id) {
	this->decision.push_back(id);
}

int Customer::getBill() const {
	return bill;
}
void Customer::addBill(int amount) {
	bill += amount;
}
//************************************************************************************************************

// Vegetarian Customer implementation
VegetarianCustomer::VegetarianCustomer(std::string name, int id): Customer(name,id) {}

VegetarianCustomer::~VegetarianCustomer() {}

//copy constructor
VegetarianCustomer::VegetarianCustomer(const VegetarianCustomer& other):Customer(other) {
}

std::vector<int> VegetarianCustomer::order(const std::vector<Dish> &menu) {
	int mealId =-1;
	int drinkId = -1;
	for (unsigned int i=0; i<menu.size(); i++) {
		if (menu.at(i).getType() == BVG) {
			if (drinkId == -1 or menu.at(drinkId).getPrice() < menu.at(i).getPrice() or (menu.at(drinkId).getPrice() == menu[i].getPrice()
			 and menu.at(drinkId).getId() > menu.at(i).getId())) {
				drinkId = menu.at(i).getId();
			}
		}

		if (menu.at(i).getType() == VEG) {
			if (mealId == -1 or menu.at(mealId).getId() > menu.at(i).getId()) {
				mealId = menu.at(i).getId();
			}
		}
	}
	if (mealId != -1) {
		addBill(menu.at(drinkId).getPrice());
		addDecision(mealId);
	}
	if (drinkId != -1) {
		addBill(menu.at(drinkId).getPrice());
		addDecision(drinkId);
	}
	if (mealId != -1 or drinkId !=-1) {
        addNumOfOrder();
		return getDecision();
	}
	return {}; //if he doesn't have anything to order
}

//don't know what I need to return...
std::string VegetarianCustomer::toString() const {
	return "id ="+std::to_string(getId())+", name="+getName();
}

Customer* VegetarianCustomer::clone(){
	return new VegetarianCustomer(*this);
}





//************************************************************************************************************

//Cheap Customer implementation
CheapCustomer::CheapCustomer(std::string name, int id): Customer(name,id) {}

CheapCustomer::~CheapCustomer() {}

//copy sontructor
CheapCustomer::CheapCustomer(const CheapCustomer& other): Customer(other){

}

std::vector<int> CheapCustomer::order(const std::vector<Dish> &menu) {
	if (getNumOfOrder() == 0) { //checks if he ordered before
		int dishId = -1; // chosen dish's index and its id
		for (unsigned int i=0; i<menu.size(); i++) {
			if (dishId == -1 or menu.at(dishId).getPrice() > menu.at(i).getPrice() or (menu.at(dishId).getPrice() == menu.at(i).getPrice()
			 and menu.at(dishId).getId() > menu.at(i).getId())) {
				dishId = i;
			}
		}
		if (dishId != -1) {
			addBill(menu.at(dishId).getPrice());
			addDecision(dishId);
			addNumOfOrder();
			return getDecision();
		}	
	}
	return {}; //if he ordered before returns null
}

//don't know what I need to return...
std::string CheapCustomer::toString() const {
	return "id ="+std::to_string(getId())+", name="+getName();
}

Customer* CheapCustomer::clone(){
	return new CheapCustomer(*this);
}

//************************************************************************************************************

//Spicy Customer implementation
SpicyCustomer::SpicyCustomer(std::string name, int id): Customer(name,id) {}

SpicyCustomer::~SpicyCustomer() {}

//copy constructor
SpicyCustomer::SpicyCustomer(const SpicyCustomer& other): Customer(other){
}

std::vector<int> SpicyCustomer::order(const std::vector<Dish> &menu) {
	int dishId = -1; // chosen dish's id
	for (unsigned int i=0; i<menu.size(); i++) {
		if (getNumOfOrder() == 0) {
			if (menu[i].getType() == SPC) { //for first order
				if (dishId == -1 or menu.at(dishId).getPrice() < menu.at(i).getPrice() or (menu.at(dishId).getPrice() == menu.at(i).getPrice()
				 and menu.at(dishId).getId() > menu.at(i).getId())) {
					dishId = menu.at(i).getId();
				}
			}
		}
		else {
			if (menu[i].getType() == BVG) { //for other order
				if (dishId == -1 or menu.at(dishId).getPrice() > menu.at(i).getPrice() or (menu.at(dishId).getPrice() == menu.at(i).getPrice()
				 and menu.at(dishId).getId() > menu.at(i).getId())) {
					dishId = menu.at(i).getId();
				}
			}	
		}
	}
	if (dishId != -1) {
		addBill(menu.at(dishId).getPrice());
		addDecision(dishId);
		addNumOfOrder();
		return getDecision();
	}
	return {}; //if he doesn't have anything to order
}

//don't know what I need to return...
std::string SpicyCustomer::toString() const {
	return "id ="+std::to_string(getId())+", name="+getName();
}

Customer* SpicyCustomer::clone(){
	return new SpicyCustomer(*this);
}

//************************************************************************************************************

//Alcoholic Customer implementation
AlchoholicCustomer::AlchoholicCustomer(std::string name, int id): Customer(name,id), prev(-1) {}

AlchoholicCustomer::~AlchoholicCustomer() {}

//copy constructor
AlchoholicCustomer::AlchoholicCustomer(const AlchoholicCustomer& other): Customer(other), prev(other.prev) {

}

std::vector<int> AlchoholicCustomer::order(const std::vector<Dish> &menu) {
	int dishId = -1; // chosen dish's id
	int menu_size = menu.size();
	for (int i = 0; i < menu_size; i++) {
		if (getNumOfOrder() == 0 or prev != menu.at(i).getId()) {
		    //makes sure it's his first order or not the same as the previous one
			if (menu.at(i).getType() == ALC) { 
				if (dishId == -1 or menu.at(dishId).getPrice() > menu.at(i).getPrice() or (menu.at(dishId).getPrice() == menu.at(i).getPrice()
				 and menu.at(dishId).getId() > menu.at(i).getId()))
				    if (prev == -1 or (i != prev and menu.at(i).getPrice() >= menu.at(prev).getPrice()))
				        //if this dish is more expensive than the prev or there is no prev
				        dishId = menu.at(i).getId();
			}
		}
	}
	if (dishId != -1) {
		addBill(menu.at(dishId).getPrice());
		addDecision(dishId);
		addNumOfOrder();
		prev = dishId;
		return getDecision();
	}
	return {}; //if he doesn't have anything to order
}

std::string AlchoholicCustomer::toString() const {
	return "id ="+std::to_string(getId())+", name="+getName();
}

Customer* AlchoholicCustomer::clone(){
	return new AlchoholicCustomer(*this);
}
