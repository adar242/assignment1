#include "../include/Table.h"
#include "../include/Customer.h"
#include "../include/Dish.h"
#include <vector>
#include <iostream>

using std::pair;
using std::vector;
//constructor 
//the table upon creation is not open
Table::Table(int capacity): capacity(capacity), open(false), bill(0), customersList(), orderList(), dishNum(0) {}

//destructor
Table::~Table () {
	for (unsigned int i=0; i<customersList.size(); i++) {
		delete customersList.at(i);
		customersList.at(i)=nullptr;
	}
}

//move constructor
Table::Table(Table&& other): capacity(other.capacity), open(other.open),
 bill(other.bill), customersList(other.customersList), orderList(other.orderList), dishNum(other.dishNum)
{
	for (unsigned int i = 0; i < other.customersList.size(); ++i){
		other.customersList.at(i) = nullptr;
	}
}

//copy constructor
Table::Table(const Table& other): capacity(other.capacity), open(other.open),
 bill(other.bill),customersList(), orderList(other.orderList), dishNum(other.dishNum) {

	for (unsigned int i=0; i<other.customersList.size(); i++) {
		Customer* customer = other.customersList.at(i)->clone();
		customersList.push_back(customer);
	}
}

//move assignment operator
Table& Table::operator=(Table &&other){
	if (this == &other){
		return *this;
	}

	//deleting customers list
	for (unsigned int i=0; i<customersList.size(); i++) {
		delete customersList.at(i);
		customersList.at(i)=nullptr;
	}

	capacity = other.capacity;
	open = other.open;
	bill = other.bill;

    for (unsigned int i = 0; i < other.orderList.size(); ++i) {
        orderList.push_back(other.orderList.at(i));
    }
        
	customersList = other.customersList;
	for (unsigned int i = 0; i < other.customersList.size(); ++i){
		other.customersList.at(i) = nullptr;
	}
	return *this;
}

//assignment operator
Table& Table::operator=(const Table &other) {
	if (this != &other) {
		capacity = other.capacity;
		open = other.open;
		bill = other.bill;

		//clearing this customerList
		for (unsigned int i=0; i<customersList.size(); i++){
			delete customersList.at(i);
		}
		customersList.clear();

		//popluatin this customerlist with new customers based on other.customerlist
		for (unsigned int i=0; i<other.customersList.size(); i++){
			Customer* cust = other.customersList.at(i) -> clone();
			customersList.push_back(cust);

		}
		
		return *this;
	}
	return *this;
}

int Table::getCapacity() const {
	return capacity;
}

void Table::addCustomer(Customer* customer) {
	bill += customer->getBill();
	customersList.push_back(customer);
}

void Table::removeCustomer(int id) {
	for (unsigned int i = 0; i < customersList.size(); i++) {
		if (customersList.at(i)->getId() == id) {
			bill -= customersList.at(i)->getBill();
			customersList.at(i) = nullptr;
			customersList.erase(customersList.begin()+i);
		}
	}
}

//if can't find the customer return null
Customer* Table::getCustomer(int id) {
	for (unsigned int i=0; i<customersList.size(); i++) {
		if (customersList.at(i)->getId() == id)
			return customersList.at(i);
	}
	return nullptr;
}

std::vector<Customer*>& Table::getCustomers() {
	return customersList;
}

std::vector<OrderPair>& Table::getOrders() {
	return orderList;
}

//takes the orders from the customers, arranges them in pairs and puts them in the field
void Table::order(const std::vector<Dish> &menu) {
	for (unsigned int i = 0; i < customersList.size(); i++) {
	    int prev_dishNum = customersList.at(i)->getDecision().size();
		std::vector<int> choices = customersList.at(i)->order(menu);
		int choices_size = choices.size();
		for (int j = prev_dishNum; j < choices_size; j++) { //takes each choice made by the customer and puts it in a pair
			bill += menu[choices.at(j)].getPrice();
			pair<int,Dish> p (customersList.at(i)->getId(), menu[choices.at(j)]);
			orderList.push_back(p);
			dishNum += customersList.at(i)->getNumOfOrder();
            std::cout << customersList.at(i)->getName()+" ordered "+menu[choices.at(j)].getName() << std::endl;
		}
	}
}

void Table::openTable() {
	open=true;
	bill=0;
}

void Table::closeTable() {
	open=false;
	orderList.clear();
	for (unsigned int i = 0; i < customersList.size(); i++) {
	    delete customersList.at(i);
	}
	customersList.clear();
}

int Table::getBill() {
	return bill;
}

bool Table::isOpen() {
	return open;
}

void Table::addOrders(std::vector <std::pair<int,Dish>> orders) {
	for (unsigned int i = 0; i < orders.size(); i++) {
		orderList.push_back(orders.at(i)); //given orders are put in the order list of this table
	}
}

std::vector <std::pair<int,Dish>> Table::removeOrders (int id) {
	std::vector <std::pair<int,Dish>> ordersToRemove; //orders of the customer that moves a table
    std::vector <std::pair<int,Dish>> ordersToSave; //orders of the other customers
	for (unsigned int i = 0; i < orderList.size(); i++) {
		if (orderList.at(i).first == id)
			ordersToRemove.push_back(orderList.at(i));
		else
			ordersToSave.push_back(orderList.at(i));
	}
	orderList.clear();
	for (unsigned int i = 0; i < ordersToSave.size(); i++) {
	    orderList.push_back(ordersToSave.at(i));
	}
	return ordersToRemove;
}

