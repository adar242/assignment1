#ifndef ACTION_H_
#define ACTION_H_

#include <string>
#include <iostream>
#include "Customer.h"

enum ActionStatus{
    PENDING, COMPLETED, ERROR
};

//Forward declaration
class Restaurant;

class BaseAction{
public:
    BaseAction();
    BaseAction(const BaseAction& other);
    BaseAction(BaseAction& other);
    virtual ~BaseAction();
    ActionStatus getStatus() const;
    virtual void act(Restaurant& restaurant)=0;
    virtual std::string toString() const=0;
    virtual BaseAction *clone()=0;
    std::string statusToString() const;
    std::string getInput_Status() const;
    void actionLogString(Restaurant& restaurant); //builds the input_status
protected:
    void complete();
    void error(std::string errorMsg);
    std::string getErrorMsg() const;
private:
    std::string errorMsg;
    ActionStatus status;
    std::string input_status; //saves the input from the user along with the status
};


class OpenTable : public BaseAction {
public:
    OpenTable(int id, std::vector<Customer *> &customersList);
    OpenTable(const OpenTable& other);
    OpenTable(OpenTable& other);
    ~OpenTable();
    void act(Restaurant &restaurant); //opens the table
    std::string toString() const; //returns the id, number of customers and their ids
    BaseAction *clone();

private:
    const int tableId;
    std::vector<Customer *> customers;
};


class Order : public BaseAction {
public:
    Order(int id);
    Order(const Order& other);
    Order(Order& other);
    ~Order();
    void act(Restaurant &restaurant); //takes the orders
    std::string toString() const;
    int customerById (int id,const std::vector<Customer*> customersList); // returns the index of a given customer based on its id
    BaseAction *clone();
private:
    const int tableId;
};


class MoveCustomer : public BaseAction {
public:
    MoveCustomer(int src, int dst, int customerId);
    MoveCustomer(const MoveCustomer& other);
    MoveCustomer(MoveCustomer& other);
    ~MoveCustomer();
    void act(Restaurant &restaurant); //if possible moves customer with given id from srcTable to dstTable
    std::string toString() const;
    bool possible(Restaurant &restaurant); //checks if it's possible to complete act
    BaseAction *clone();
private:
    const int srcTable;
    const int dstTable;
    const int id;
};


class Close : public BaseAction {
public:
    Close(int id);
    Close(const Close& other);
    Close(Close& other);
    ~Close();
    void act(Restaurant &restaurant);
    std::string toString() const;
    BaseAction *clone();
private:
    const int tableId;
};


class CloseAll : public BaseAction {
public:
    CloseAll();
    CloseAll(const CloseAll& other);
    CloseAll(CloseAll& other);
    ~CloseAll();
    void act(Restaurant &restaurant);
    std::string toString() const;
    BaseAction *clone();
private:
};


class PrintMenu : public BaseAction {
public:
    PrintMenu();
    PrintMenu(const PrintMenu& other);
    PrintMenu(PrintMenu& other);
    ~PrintMenu();
    void act(Restaurant &restaurant);
    std::string toString() const;
    BaseAction *clone();
    std::string typeToString(DishType type);
private:
};


class PrintTableStatus : public BaseAction {
public:
    PrintTableStatus(int id);
    PrintTableStatus(const PrintTableStatus& other);
    PrintTableStatus(PrintTableStatus& other);
    ~PrintTableStatus();
    void act(Restaurant &restaurant);
    std::string toString() const;
    BaseAction *clone();
private:
    const int tableId;
};


class PrintActionsLog : public BaseAction {
public:
    PrintActionsLog();
    PrintActionsLog(const PrintActionsLog& other);
    PrintActionsLog(PrintActionsLog& other);
    ~PrintActionsLog();
    void act(Restaurant &restaurant);
    std::string toString() const;
    BaseAction *clone();
private:
};


class BackupRestaurant : public BaseAction {
public:
    BackupRestaurant();
    BackupRestaurant(const BackupRestaurant& other);
    BackupRestaurant(BackupRestaurant& other);
    ~BackupRestaurant();
    void act(Restaurant &restaurant);
    std::string toString() const;
    BaseAction *clone();
private:
};


class RestoreResturant : public BaseAction {
public:
    RestoreResturant();
    RestoreResturant(const RestoreResturant& other);
    RestoreResturant(RestoreResturant& other);
    ~RestoreResturant();
    void act(Restaurant &restaurant);
    std::string toString() const;
    BaseAction *clone();

};


#endif