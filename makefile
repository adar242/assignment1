#All Targets
all: assignment1

# Tool invocations
# Executable "assignment1" depends on the compiled src files
assignment1: bin/Customer.o bin/Table.o  bin/Dish.o bin/Main.o bin/Action.o bin/Restaurant.o 
	@echo 'Building target: assignment1'
	@echo 'Invoking: C++ Linker'
	g++ -o bin/assignment1 bin/Customer.o bin/Dish.o bin/Main.o bin/Action.o bin/Restaurant.o bin/Table.o
	@echo 'Finished building target: assignment1'
	@echo ' '

# Depends on the source and header files
bin/Customer.o: src/Customer.cpp
	g++ -g -Wall -Weffc++ -std=c++11 -c -Iinclude -o bin/Customer.o src/Customer.cpp

bin/Table.o: src/Table.cpp
	g++ -g -Wall -Weffc++ -std=c++11 -c -Iinclude -o bin/Table.o src/Table.cpp

# Depends on the source and header files 
bin/Dish.o: src/Dish.cpp
	g++ -g -Wall -Weffc++ -std=c++11 -c -Iinclude -o bin/Dish.o src/Dish.cpp
	
bin/Main.o: src/Main.cpp
	g++ -g -Wall -Weffc++ -std=c++11 -c -Iinclude -o bin/Main.o src/Main.cpp
	
bin/Action.o: src/Action.cpp
	g++ -g -Wall -Weffc++ -std=c++11 -c -Iinclude -o bin/Action.o src/Action.cpp

bin/Restaurant.o: src/Restaurant.cpp
	g++ -g -Wall -Weffc++ -std=c++11 -c -Iinclude -o bin/Restaurant.o src/Restaurant.cpp
	


#Clean the build directory
clean: 
	rm -f bin/*
