#include "../include/Action.h"
#include "../include/Restaurant.h"
#include "../include/Table.h"
#include "../include/Dish.h"
#include <iostream>

using std::vector;

//constructor
BaseAction::BaseAction(): errorMsg(), status(PENDING), input_status() {}

// Copy Constructor
BaseAction::BaseAction(const BaseAction& other): errorMsg(other.errorMsg),
status(other.status), input_status(other.input_status) {}

// Move Constructor
BaseAction::BaseAction(BaseAction& other): errorMsg(other.errorMsg),
status(other.status), input_status(other.input_status) {}

// Destructor
BaseAction::~BaseAction() {}

ActionStatus BaseAction::getStatus() const {
	return status;
}

void BaseAction::complete() {
	status = COMPLETED;
}

void BaseAction::error (std::string errorMsg) {
	status = ERROR;
	(*this).errorMsg = errorMsg;
	std::cout << "Error: "+errorMsg << std::endl;
}

std::string BaseAction::getErrorMsg() const {
	return errorMsg;
};

std::string BaseAction::statusToString() const {
	if (status == PENDING)
		return "Pending";
	if (status == COMPLETED)
		return "Completed";
	return "Error";
}

std::string BaseAction::getInput_Status() const {
	return input_status;
}

void BaseAction::actionLogString(Restaurant& restaurant) {
	input_status += restaurant.get_userInput()+" "+statusToString();
	if (statusToString() == "Error")
		input_status += ": "+errorMsg;
}

//************************************************************************************************************

// OpenTable implementation
// Constructor
OpenTable::OpenTable (int id, std::vector<Customer *> &customersList): BaseAction(), tableId(id), customers(customersList) {}

// Copy Constructor
OpenTable::OpenTable (const OpenTable& other): BaseAction(other), tableId(other.tableId), customers() {
	for (unsigned int i=0; i<other.customers.size(); i++) {
		customers.push_back(other.customers.at(i));
	}

}

// Move Constructor
OpenTable::OpenTable (OpenTable& other): BaseAction(other), tableId(other.tableId), customers() {
	for (unsigned int i=0; i<other.customers.size(); i++) {
		customers.push_back(other.customers.at(i));
	}
}

// Destructor
OpenTable::~OpenTable () {
	for (unsigned int i=0; i < customers.size(); i++) {
		customers.at(i) = nullptr;
	}
}

void OpenTable::act (Restaurant &restaurant) {
	if (restaurant.getNumOfTables() > tableId and !restaurant.getTable(tableId)->isOpen()) {
		Table* chosenTable = restaurant.getTable(tableId);
		int customers_size =customers.size();
		if (customers_size <= chosenTable->getCapacity()) {
			chosenTable->openTable();
			for (int i = 0; i < customers_size; i++)
				chosenTable->addCustomer(customers.at(i));
			complete();
		}
		else
		    error("Table doesn't have enough seats");
		chosenTable = nullptr;
	}
	else
	    error("Table does not exist or is already open");
	actionLogString(restaurant);
}

std::string OpenTable::toString () const {
	std::string ids="";
	for (unsigned int i=0; i<customers.size(); i++) {
		ids+=std::to_string(customers.at(i)->getId())+", ";
	}
	return "tableId ="+std::to_string(tableId)+", amount of customers:"+ std::to_string(customers.size())+", customers' id:"+ids ;
}

BaseAction* OpenTable::clone() {
	return new OpenTable (*this);
}




//************************************************************************************************************

// Order implementation
// Constructor
Order::Order (int id): BaseAction(), tableId(id) {}

// Copy Constructor
Order::Order (const Order& other): BaseAction(other), tableId(other.tableId) {}

// Move Constructor
Order::Order (Order& other): BaseAction(other), tableId(other.tableId) {}

// Destructor
Order::~Order() {}

BaseAction* Order::clone() {
	return new Order(*this);
}

void Order::act(Restaurant &restaurant) {
	if (restaurant.getNumOfTables() > tableId and restaurant.getTable(tableId)->isOpen()) {
		Table* chosenTable = restaurant.getTable(tableId);
		chosenTable->order(restaurant.getMenu());
		/*for (unsigned int i=chosenTable->; i<chosenTable->getOrders().size(); i++) { //gets the indexes of each customer and his dish and then prints their order
			int customerId = chosenTable->getOrders().at(i).first;
			int dishId = chosenTable->getOrders().at(i).second.getId();
		}*/
		complete();
		actionLogString(restaurant);
		return;
	}
	error("Table does not exist or is not open");
	actionLogString(restaurant);
}

std::string Order::toString() const {
	return "table "+std::to_string(tableId)+" order status "+ statusToString();
}

int Order::customerById (int id,const std::vector<Customer*> customersList) {
	for (unsigned int i=0; i<customersList.size(); i++) {
		if (customersList.at(i)->getId() == id)
			return i;
	}
	return -1;
}

//************************************************************************************************************

// MoveCustomer implementation
// Constructor
MoveCustomer::MoveCustomer (int src, int dst, int customerId): BaseAction(), srcTable(src), dstTable(dst), id(customerId) {}

// Copy Constructor
MoveCustomer::MoveCustomer(const MoveCustomer& other): BaseAction(other), srcTable(other.srcTable), dstTable(other.dstTable), id(other.id) {}

// Move Constructor
MoveCustomer::MoveCustomer(MoveCustomer& other): BaseAction(other), srcTable(other.srcTable), dstTable(other.dstTable), id(other.id) {}

// Destructor
MoveCustomer::~MoveCustomer() {}

BaseAction* MoveCustomer::clone() {
	return new MoveCustomer(*this);
}

void MoveCustomer::act(Restaurant &restaurant) {
	if (!possible(restaurant)) {
		error("Cannot move customer");
		actionLogString(restaurant);
		return;
	}
	Table* src = restaurant.getTable(srcTable);
	Table* dst = restaurant.getTable(dstTable);
	dst->addCustomer(src->getCustomer(id));
	src->removeCustomer(id);
	dst->addOrders(src->removeOrders(id));
	if (src->getCustomers().size() == 0)
	    src->closeTable();
	src = nullptr;
	dst = nullptr;
	complete();
	actionLogString(restaurant);
}

bool MoveCustomer::possible(Restaurant &restaurant) {
	bool ans=true;
	if (restaurant.getNumOfTables() < srcTable or restaurant.getNumOfTables() < dstTable)
		ans= false;
	Table* src = restaurant.getTable(srcTable);
	Table* dst = restaurant.getTable(dstTable);
	int dstCustomers_size = dst->getCustomers().size();
	if (src->isOpen() == false or dst->isOpen() == false)
		ans= false;
	if (src->getCustomer(id) == nullptr)
		ans= false;
	if (dst->getCapacity() == dstCustomers_size)
		ans= false;
	src = nullptr;
	dst = nullptr;
	return ans;
}

std::string MoveCustomer::toString() const {
	std::string returnStatus;
	return "move customer id "+std::to_string(id)+" status "+statusToString();
}

//************************************************************************************************************

// Close implementation
// Constructor
Close::Close (int id): BaseAction(),tableId(id) {}

// Copy Constructor
Close::Close (const Close& other): BaseAction(other), tableId(other.tableId) {}

// Move Constructor
Close::Close (Close& other): BaseAction(other), tableId(other.tableId) {}

// Destructor
Close::~Close() {}

BaseAction* Close::clone() {
	return new Close(*this);
}

void Close::act (Restaurant &restaurant) {
	if (restaurant.getNumOfTables() < tableId or restaurant.getTable(tableId)->isOpen() == false) {
		error("Table does not exist or is not open");
		actionLogString(restaurant);
		return;
	}
	Table* cTable = restaurant.getTable(tableId);
	std::cout  << "Table "+std::to_string(tableId)+" was closed. Bill "
	+std::to_string(cTable->getBill())+"NIS" << std::endl;
	cTable->closeTable();
	cTable = nullptr;
	complete();
	actionLogString(restaurant);
}

std::string Close::toString() const {
	return "close table "+std::to_string(tableId)+" status "+statusToString();
}

//************************************************************************************************************

// CloseAll implementation
// Constructor
CloseAll::CloseAll (): BaseAction() {}

// Copy Constructor
CloseAll::CloseAll(const CloseAll& other): BaseAction(other) {}

// Move Constructor
CloseAll::CloseAll(CloseAll& other): BaseAction(other) {}

// Destructor
CloseAll::~CloseAll() {}

BaseAction* CloseAll::clone() {
	return new CloseAll(*this);
}

void CloseAll::act(Restaurant &restaurant) {
	for (int i=0; i<restaurant.getNumOfTables(); i++) {
        if (restaurant.getTable(i)->isOpen() == true) {
            Close *closeAction = new Close(i);
            closeAction->act(restaurant);
            delete closeAction;
        }
    }
    extern Restaurant* backup;
	if (backup != nullptr) {
	    backup = nullptr;
	}
	complete();
	actionLogString(restaurant);
}

std::string CloseAll::toString() const {
	return "close all tables status "+statusToString();
}

//************************************************************************************************************

// CloseAll implementation
// Constructor
PrintMenu::PrintMenu(): BaseAction() {}

// Copy Constructor
PrintMenu::PrintMenu(const PrintMenu& other): BaseAction(other) {}

// Move Constructor
PrintMenu::PrintMenu(PrintMenu& other): BaseAction(other) {}

// Destructor
PrintMenu::~PrintMenu() {}

BaseAction* PrintMenu::clone() {
	return new PrintMenu(*this);
}

void PrintMenu::act(Restaurant &restaurant) {
	const std::vector<Dish> &menu= restaurant.getMenu();
	for (unsigned int i=0; i<menu.size(); i++) {
		std::string type = typeToString(menu.at(i).getType());
		std::cout << menu.at(i).getName()+" "+type+" "+std::to_string(menu.at(i).getPrice())+"NIS" << std::endl;
	}
	complete();
	actionLogString(restaurant);
}

std::string PrintMenu::toString() const {
	return "print menu status "+statusToString();
}

std::string PrintMenu::typeToString(DishType type) {
	if (type == VEG)
		return "VEG";
	if (type == SPC)
		return "SPC";
	if (type == BVG)
		return "BVG";
	return "ALC";
}

//************************************************************************************************************

// PrintTableStatus implementation
// Constructor
PrintTableStatus::PrintTableStatus(int id): BaseAction(), tableId(id) {}

// Copy Constructor
PrintTableStatus::PrintTableStatus(const PrintTableStatus& other): BaseAction(other), tableId(other.tableId) {}

// Move Constructor
PrintTableStatus::PrintTableStatus(PrintTableStatus& other): BaseAction(other), tableId(other.tableId) {}

// Destructor
PrintTableStatus::~PrintTableStatus() {}

BaseAction* PrintTableStatus::clone() { 
	return new PrintTableStatus(*this);
}
void PrintTableStatus::act(Restaurant &restaurant) {
	Table* myTable = restaurant.getTable(tableId);
	std::string ans = "";
	if (myTable->isOpen())
	    ans = "open";
	else
	    ans = "closed";

	std::cout << "Table "+std::to_string(tableId)+" status: "+ans << std::endl;
	if (myTable->isOpen()) {
		std::cout << "Customers:" << std::endl;
		for (unsigned int i=0; i<myTable->getCustomers().size(); i++)
			std::cout << std::to_string(myTable->getCustomers().at(i)->getId())+" "+myTable->getCustomers().at(i)->getName() << std::endl;
		std::cout << "Orders:" << std::endl;
		for (unsigned int j=0; j<myTable->getOrders().size(); j++) {
			std::pair<int, Dish> order = myTable->getOrders().at(j);
			std::cout << order.second.getName()+" "+std::to_string(order.second.getPrice())+"NIS "+std::to_string(order.first) << std::endl;
		}
		std::cout << "Current Bill: "+std::to_string(myTable->getBill())+"NIS" << std::endl;
	}
	myTable = nullptr;
	complete();
	actionLogString(restaurant);
}

std::string PrintTableStatus::toString() const {
	return "Print table status: "+statusToString();
}

//************************************************************************************************************

// PrintActionsLog implementation
// Constructor
PrintActionsLog::PrintActionsLog(): BaseAction() {}

// Copy Constructor
PrintActionsLog::PrintActionsLog(const PrintActionsLog& other): BaseAction(other) {}

// Move Constructor
PrintActionsLog::PrintActionsLog(PrintActionsLog& other): BaseAction(other) {}

// Destructor
PrintActionsLog::~PrintActionsLog() {}

BaseAction* PrintActionsLog::clone() {
	return new PrintActionsLog(*this);
}

void PrintActionsLog::act(Restaurant &restaurant) {
	for (unsigned int i=0; i<restaurant.getActionsLog().size(); i++)
		std::cout << restaurant.getActionsLog().at(i)->getInput_Status() << std::endl;
	complete();
	actionLogString(restaurant);
}
std::string PrintActionsLog::toString() const {
	return "print actions log status: "+statusToString();
}

//************************************************************************************************************

// BackupRestaurant implementation
// Constructor
BackupRestaurant::BackupRestaurant(): BaseAction() {}

// Copy Constructor
BackupRestaurant::BackupRestaurant(const BackupRestaurant& other): BaseAction(other) {}

// Move Constructor
BackupRestaurant::BackupRestaurant(BackupRestaurant& other): BaseAction(other) {}

// Destructor
BackupRestaurant::~BackupRestaurant() {}

BaseAction* BackupRestaurant::clone() {
	return new BackupRestaurant(*this);
}

void BackupRestaurant::act(Restaurant &restaurant) {
	extern Restaurant* backup;
    complete();
    actionLogString(restaurant);
	backup = new Restaurant(restaurant);
}
std::string BackupRestaurant::toString() const {
	return "backup restaurant status: "+statusToString();
}

//************************************************************************************************************

// RestoreRestaurant implementation
// Constructor
RestoreResturant::RestoreResturant(): BaseAction() {}

// Copy Constructor
RestoreResturant::RestoreResturant(const RestoreResturant& other): BaseAction(other) {}

// Move Constructor
RestoreResturant::RestoreResturant(RestoreResturant& other): BaseAction(other) {}

// Destructor
RestoreResturant::~RestoreResturant() {}

BaseAction* RestoreResturant::clone() {
	return new RestoreResturant(*this);
}

void RestoreResturant::act(Restaurant &restaurant) {
	extern Restaurant* backup;
	if (backup == nullptr) {
		error("No backup available");
		actionLogString(restaurant);
		return;
	}
	restaurant = (*backup);
	complete();
	actionLogString(restaurant);
}

std::string RestoreResturant::toString() const {
	return "restore restaurant status: "+statusToString();
}